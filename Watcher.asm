; Tested on Digi-Watcher.com's Watcher 2.35
DWWatcher_CodeGen proto MagicValue: DWORD, OutBuffer: DWORD

.code
Gen_Watcher proc in_username: DWORD, out_serial: DWORD
    local Magic: DWORD

    invoke StrLen, in_username
    mov ecx, 345
    imul eax, ecx
    cdq
    mov ebx, 10000
    idiv ebx
    mov Magic, edx

    invoke StrLen, in_username
    mov esi, eax
    xor ecx, ecx
    inc ecx
    .while ecx <= esi
        mov eax, ecx
        add eax, 3
        imul eax, ecx
        mov ebx, eax
        mov eax, ecx
        mov edi, 7
        cdq
        idiv edi
        mov eax, in_username
        movzx eax, byte ptr ds:[eax+ecx-1]
        mov edi, eax
        imul eax, edx
        add eax, ebx
        imul eax, edi
        add Magic, eax
        inc ecx
    .endw

    mov eax, Magic
    .while eax > 99999
        mov ebx, 3
        cdq
        idiv ebx
    .endw
    mov Magic, eax

    .while eax < 1000
        add eax, eax
    .endw
    mov Magic, eax

    invoke DWWatcher_CodeGen, Magic, out_serial
    Ret
Gen_Watcher endp

DWWatcher_CodeGen proc MagicValue: DWORD, OutBuffer: DWORD
    local var1: BYTE
    local var2: DWORD
    local var3: DWORD

    mov eax, OutBuffer
    mov dword ptr ds:[eax], 69676944h
    mov dword ptr ds:[eax+4], 7461572Dh
    mov dword ptr ds:[eax+8], 72656863h
    mov dword ptr ds:[eax+12], 6D6F632Eh
    mov byte ptr ds:[eax+16], 2Fh
    mov edi, MagicValue
    mov eax, 66666667h
    imul edi
    mov ebx, 5
    sar edx, 2
    mov eax, edx
    shr eax, 1Fh
    mov ecx, OutBuffer
    add ecx, 11h
    lea eax, dword ptr ds:[edx+eax+25h]
    mov var2, 00h
    mov var3, eax

    .repeat
        cdq
        mov esi, 4Bh
        idiv esi
        add dl, 2Eh
        mov var1, dl
        mov byte ptr ds:[ecx], dl
        .if dl > 50h
            mov eax, ebx
            lea esi, dword ptr ds:[ebx+5]
            imul eax, ebx
            add eax, edi
            cdq
            idiv esi
            mov al, var1
            sub al, dl
            mov byte ptr ds:[ecx], al
        .endif
        .if byte ptr ds:[ecx] < 6Eh
            lea eax, dword ptr ds:[ebx-1]
            mov esi, var2
            imul eax, eax
            add eax, esi
            mov esi, 0Bh
            add eax, edi
            cdq
            idiv esi
            add dl, byte ptr ds:[ecx]
            mov byte ptr ds:[ecx], dl
        .endif
        mov eax, edi
        lea esi, dword ptr ds:[ebx-1]
        cdq
        idiv esi
        mov esi, edx
        .if esi == 3
            mov eax, ebx
            push ebp
            mov ebp, 13h
            imul eax, ebx
            add eax, edi
            cdq
            idiv ebp
            pop ebp
            add dl, 32h
            mov byte ptr ds:[ecx], dl
        .endif
        .if esi == 1
            mov eax, ebx
            mov esi, 0Fh
            imul eax, ebx
            add eax, edi
            cdq
            idiv esi
            mov al, 78h
            sub al, dl
            mov byte ptr ds:[ecx], al
        .endif
        mov al, byte ptr ds:[ecx]
        .if al > 39h && al < 41h
            mov dl, bl
            add dl, 3Ch
            mov byte ptr ds:[ecx], dl
        .endif
        mov al, byte ptr ds:[ecx]
        .if al > 5Ah && al < 61h
            mov al, 5Fh
            sub al, bl
            mov byte ptr ds:[ecx], al
        .endif
        .if byte ptr ds:[ecx] == 31h
            mov dl, 3Eh
            sub dl, bl
            mov byte ptr ds:[ecx], dl
        .endif
        mov al, byte ptr ds:[ecx]
        .if al >= 61h && al <= 7Ah
            sub al, 20h
            mov byte ptr ds:[ecx], al
        .endif
        .if byte ptr ds:[ecx] == 49h
            mov al, bl
            add al, 2Bh
            mov byte ptr ds:[ecx], al
        .endif
        mov eax, var3
        mov esi, var2
        add eax, 28h
        add esi, 4
        inc ebx
        inc ecx
        mov var3, eax
        mov var2, esi
        lea edx, dword ptr ds:[ebx-5]
    .until edx >= 0Ah
    Ret
DWWatcher_CodeGen endp

.code
Gen_ThigoLilKeygenMe proc in_username: DWORD, out_serial: DWORD
    local hFile: DWORD
    local BytesWritten: DWORD

    invoke wsprintf, offset ThKM_TempString, offset ThKM_SrllFormat, in_username

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax

    invoke szRev, offset ThKM_TempString, offset ThKM_TempString
    invoke StrLen, offset ThKM_TempString
    invoke bnFromBytes, offset ThKM_TempString, eax, BigC, FALSE
    invoke bnFromHex, offset ThKM_D, BigD
    invoke bnFromHex, offset ThKM_N, BigN
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    invoke bnShl, BigM, 10h                 ;Hack to work around Drizz's library's "I only work with DWORDs" limitation
    invoke bnToBytesEx, BigM, out_serial
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish

    invoke CreateFile, offset ThKM_FileName, GENERIC_WRITE, NULL,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL
    mov hFile, eax
    invoke WriteFile, hFile, out_serial, 1Eh, addr BytesWritten, NULL
    invoke CloseHandle, hFile
    Ret
Gen_ThigoLilKeygenMe endp

.code
Gen_XylitolBarbecueMe proc in_username: DWORD, out_serial: DWORD
    local NameLen: DWORD

    invoke RtlZeroMemory, offset XyliBBQMe_TStr, sizeof XyliBBQMe_TStr
    invoke RtlZeroMemory, offset XyliBBQMe_BFisS, sizeof XyliBBQMe_BFisS

    invoke StrLen, in_username
    mov NameLen, eax
    invoke HexEncode, in_username, NameLen, offset XyliBBQMe_TStr
    mov eax, NameLen
    mov ecx, eax
    add ecx, ecx
    add ecx, offset XyliBBQMe_TStr
    imul eax, 539h
    invoke wsprintf, ecx, offset XyliBBQMe_Hexws, eax
    invoke BlowfishInit, offset XyliBBQMe_BFisS, 00h
    invoke BlowfishEncrypt, offset XyliBBQMe_TStr, offset XyliBBQMe_BFisS
    invoke HexEncode, offset XyliBBQMe_BFisS, NameLen, offset XyliBBQMe_TStr
    invoke SHA256Init
    mov eax, NameLen
    add eax, eax
    invoke SHA256Update, offset XyliBBQMe_TStr, eax
    invoke SHA256Final
    invoke HexEncode, eax, SHA256_DIGESTSIZE, out_serial
    invoke szCatStr, out_serial, offset XyliBBQMe_BBQSt
    Ret
Gen_XylitolBarbecueMe endp

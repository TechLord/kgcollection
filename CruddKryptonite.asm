CruKry_Encrypt proto pInOutBuffer: DWORD, pKey: DWORD, RoundsNumber: DWORD

.code
Gen_CruddKryptonite proc in_username: DWORD, out_serial: DWORD
    invoke StrLen, in_username
    .if eax < 8
        add eax, in_username
        mov byte ptr ds:[eax], 80h
    .endif
    invoke CruKry_Encrypt, in_username, offset CruKry_Key, 32
    invoke Base64Encode, in_username, 8, out_serial
    Ret
Gen_CruddKryptonite endp

CruKry_Encrypt proc pInOutBuffer: DWORD, pKey: DWORD, RoundsNumber: DWORD
    pushad
    mov edi, pInOutBuffer
    mov esi, pKey
    mov ebx, dword ptr ds:[edi]
    mov ecx, dword ptr ds:[edi+4]
    xor eax, eax
    .while RoundsNumber > 0
        add eax, 9E3779B9h
        mov edx, ecx
        shl edx, 4
        add ebx, edx
        mov edx, dword ptr ds:[esi]
        xor edx, ecx
        add ebx, edx
        mov edx, ecx
        shr edx, 5
        xor edx, eax
        add ebx, edx
        add ebx, dword ptr ds:[esi+4]
        mov edx, ebx
        shl edx, 4
        add ecx, edx
        mov edx, dword ptr ds:[esi+8]
        xor edx, ebx
        add ecx, edx
        mov edx, ebx
        shr edx, 5
        xor edx, eax
        add ecx, edx
        add ecx, dword ptr ds:[esi+0Ch]
        dec RoundsNumber
    .endw
    mov dword ptr ds:[edi], ebx
    mov dword ptr ds:[edi+4], ecx
    add edi, 8
    mov byte ptr ds:[edi], 0
    popad
    Ret
CruKry_Encrypt endp

.code
Gen_cuPegasusSplishSplash proc in_username: DWORD, out_serial: DWORD
    invoke StrLen, in_username
    mov ecx, eax
    mov esi, in_username
    mov edx, out_serial
    xor eax, eax
    xor ebx, ebx

    .while ecx > 0
        mov al, byte ptr ds:[ecx+esi-1]

        @@:
        inc ebx
        xor eax, ebx
        cmp al, 44h
        jb @b
        cmp al, 4Dh
        ja @b
        sub al, 14h
        mov byte ptr ds:[ecx+edx-1], al
        dec ecx
    .endw
    Ret
Gen_cuPegasusSplishSplash endp

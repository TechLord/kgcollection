.code
Gen_ArTeamCrackme1 proc out_serial: DWORD
    xor eax, eax
    xor ebx, ebx
    xor ecx, ecx
    mov edx, out_serial
    .while ebx < 1284h
        invoke random, 23
        movzx eax, byte ptr [Ar_Alpha+eax]
        add ebx, eax
        mov byte ptr [edx+ecx], al
        inc ecx
    .endw
    add ebx, ecx
    sub ebx, 1284h
    xor eax, eax
    .while ebx != 0
        .if eax >= ecx
            mov eax, 0
        .endif
        sub byte ptr [edx+eax], 1
        dec ebx
        inc eax
    .endw
    Ret
Gen_ArTeamCrackme1 endp

; Tested on Reversing Labs RLPack FULL v1.21

.code
Gen_RLPack proc in_username: DWORD, out_serial: DWORD
    local LicenseLevel: DWORD
    local HexSerial: DWORD
    local SerialLen: DWORD
    local hFile: DWORD
    local BytesWritten: DWORD

    ; 1 = Personal
    ; 2 = Developer
    ; 3 = Company
    mov LicenseLevel, "3"

    invoke RtlZeroMemory, offset RL_CattedString, sizeof RL_CattedString
    invoke szCatStr, offset RL_CattedString, in_username
    invoke szCatStr, offset RL_CattedString, addr LicenseLevel
    invoke StrLen, offset RL_CattedString
    mov ecx, eax
    mov eax, offset RL_CattedString
    xor ebx, ebx
    cdq
    .repeat
        mov dl, byte ptr [eax]
        rol ebx, 07
        xor bl, dl
        inc eax
        dec ecx
    .until ecx == 0
    ror ebx, 3
    mov HexSerial, ebx
    invoke wsprintf, out_serial, offset RL_SerialFormat, in_username, addr LicenseLevel, HexSerial
    mov SerialLen, eax
    invoke CreateFile, offset RL_FileName, GENERIC_WRITE, NULL,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL
    mov hFile, eax
    invoke WriteFile, hFile, out_serial, SerialLen, addr BytesWritten, NULL
    invoke CloseHandle, hFile
    Ret
Gen_RLPack endp

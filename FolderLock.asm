; Tested on NewSoftwares.net Folder Lock 7.6.0

; Classical couple of words about this.
; You might have noticed I hardly ever keep ripped code just like that.
; I usually rework it a bit and make it look presentable. Well, today
; I don't have neither the time nor the will to do so. Pure code ripping right here.
; Not that you're missing much anyway. Take the Serial Number, which could be
; anything actually, even though vendor-supplied ones look like F7-yyyymmdd-N-NNNNNN
; with N being random? numbers. Prepend and append to said Serial Number a fixed string,
; calc the hash using the custom hash function and that's it, the hash is your
; Registration Code. BTW your Master Password is stored here:
; HKEY_CURRENT_USER\Software\Microsoft\Windows\Reserved\MP
; It can be easily recovered, just in case... I haven't given a look at how Encrypted
; Lockers work, but the Lock Folders feature is quite a joke.

FL_HashInit proto ptrState: DWORD
FL_HashUpdate proto ptrState: DWORD, ptrBuff: DWORD, buffLen: DWORD
FL_HashFinal proto ptrState: DWORD
FL_HashRound proto ptrState: DWORD

.code
Gen_FolderLock proc in_username: DWORD, out_serial: DWORD
    invoke FL_HashInit, offset FL_HashState
    invoke StrLen, offset FL_FixedString
    invoke FL_HashUpdate, offset FL_HashState, offset FL_FixedString, eax
    invoke StrLen, in_username
    invoke FL_HashUpdate, offset FL_HashState, in_username, eax
    invoke StrLen, offset FL_FixedString
    invoke FL_HashUpdate, offset FL_HashState, offset FL_FixedString, eax
    invoke FL_HashFinal, offset FL_HashState
    mov eax, dword ptr ds:[FL_HashState+16]
    mov ebx, dword ptr ds:[FL_HashState+20]
    mov ecx, dword ptr ds:[FL_HashState+04]
    mov edx, dword ptr ds:[FL_HashState+08]
    mov esi, dword ptr ds:[FL_HashState+12]
    invoke wsprintf, out_serial, offset FL_SerialFormat, eax, ebx, ecx, edx, esi
    Ret
Gen_FolderLock endp

FL_HashInit proc ptrState: DWORD
    mov esi, ptrState
    xor ebx, ebx
    mov dword ptr ds:[esi+18h], ebx
    mov dword ptr ds:[esi+1ch], ebx
    mov dword ptr ds:[esi+60h], ebx
    mov dword ptr ds:[esi+04h], 0ABC02AF0h
    mov dword ptr ds:[esi+08h], 0A34ABC76h
    mov dword ptr ds:[esi+0Ch], 01DB9C5D8h
    mov dword ptr ds:[esi+10h], 0FECE236Ah
    mov dword ptr ds:[esi+14h], 04F29A421h
    mov byte ptr ds:[esi+64], bl
    mov byte ptr ds:[esi+65], bl
    Ret
FL_HashInit endp

FL_HashUpdate proc ptrState: DWORD, ptrBuff: DWORD, buffLen: DWORD
    PUSH EDX
    MOV EAX,buffLen
    MOV EDX,ptrBuff
    MOV ESI,ptrState
    PUSH EDI
    MOV EDI,EAX
    TEST EDI,EDI
    JE SHORT @L00000005
    CMP BYTE PTR DS:[ESI+64h],0
    JNZ SHORT @L00000006
    CMP BYTE PTR DS:[ESI+65h],0
    JNZ SHORT @L00000006
    PUSH EBX
    MOV EBX,1
    MOV EDI,EDI

@L00000001:
    SUB EDI,EBX
    CMP BYTE PTR DS:[ESI+65h],0
    JNZ SHORT @L00000004
    MOV EAX,DWORD PTR DS:[ESI+60h]
    MOV CL,BYTE PTR SS:[EDX]
    MOV BYTE PTR DS:[EAX+ESI+20h],CL
    ADD DWORD PTR DS:[ESI+60h],EBX
    ADD DWORD PTR DS:[ESI+18h],8
    MOV EAX,DWORD PTR DS:[ESI+60h]
    JNZ SHORT @L00000002
    ADD DWORD PTR DS:[ESI+1Ch],EBX
    JNZ SHORT @L00000002
    MOV BYTE PTR DS:[ESI+65h],BL

@L00000002:
    CMP EAX,40h
    JNZ SHORT @L00000003
    push edx
    invoke FL_HashRound, ESI
    pop edx

@L00000003:
    ADD EDX,EBX
    TEST EDI,EDI
    JNZ SHORT @L00000001

@L00000004:
    POP EBX

@L00000005:
    POP EDI
    POP EDX
    Ret

@L00000006:
    POP EDI
    MOV BYTE PTR DS:[ESI+65h],1
    POP EDX
    Ret
FL_HashUpdate endp

FL_HashFinal proc ptrState: DWORD
    mov esi, ptrState
    MOV EAX,DWORD PTR DS:[ESI+60h]
    PUSH EBX
    PUSH EDI
    MOV BYTE PTR DS:[EAX+ESI+20h],80h
    MOV EDI,1
    ADD DWORD PTR DS:[ESI+60h],EDI
    CMP EAX,37h
    MOV EAX,DWORD PTR DS:[ESI+60h]
    JLE SHORT @L00000004
    MOV ECX,40h
    XOR BL,BL
    CMP EAX,ECX
    JGE SHORT @L00000002

@L00000001:
    MOV BYTE PTR DS:[EAX+ESI+20h],BL
    ADD DWORD PTR DS:[ESI+60h],EDI
    MOV EAX,DWORD PTR DS:[ESI+60h]
    CMP EAX,ECX
    JL SHORT @L00000001

@L00000002:
    invoke FL_HashRound, ESI
    MOV ECX,38h
    CMP DWORD PTR DS:[ESI+60h],ECX
    JGE SHORT @L00000006

@L00000003:
    MOV EAX,DWORD PTR DS:[ESI+60h]
    MOV BYTE PTR DS:[ESI+EAX+20h],BL
    ADD DWORD PTR DS:[ESI+60h],EDI
    CMP DWORD PTR DS:[ESI+60h],ECX
    JL SHORT @L00000003
    JMP SHORT @L00000006

@L00000004:
    MOV ECX,38h
    CMP EAX,ECX
    JGE SHORT @L00000006
    XOR BL,BL
    NOP

@L00000005:
    MOV BYTE PTR DS:[EAX+ESI+20h],BL
    ADD DWORD PTR DS:[ESI+60h],EDI
    MOV EAX,DWORD PTR DS:[ESI+60h]
    CMP EAX,ECX
    JL SHORT @L00000005

@L00000006:
    MOVZX ECX,BYTE PTR DS:[ESI+1Fh]
    MOVZX EDX,BYTE PTR DS:[ESI+1Eh]
    MOVZX EAX,BYTE PTR DS:[ESI+1Dh]
    MOV BYTE PTR DS:[ESI+58h],CL
    MOVZX ECX,BYTE PTR DS:[ESI+1Ch]
    MOV BYTE PTR DS:[ESI+59h],DL
    MOVZX EDX,BYTE PTR DS:[ESI+1Bh]
    MOV BYTE PTR DS:[ESI+5Ah],AL
    MOVZX EAX,BYTE PTR DS:[ESI+1Ah]
    MOV BYTE PTR DS:[ESI+5Bh],CL
    MOVZX ECX,BYTE PTR DS:[ESI+19h]
    MOV BYTE PTR DS:[ESI+5Ch],DL
    MOVZX EDX,BYTE PTR DS:[ESI+18h]
    MOV BYTE PTR DS:[ESI+5Dh],AL
    MOV BYTE PTR DS:[ESI+5Eh],CL
    MOV BYTE PTR DS:[ESI+5Fh],DL
    invoke FL_HashRound, ESI
    POP EDI
    POP EBX
    Ret
FL_HashFinal endp

FL_HashRound proc ptrState: DWORD
    sub esp, 148h
    push ebx
    push ebp
    push esi
    push edi

    mov edi, ptrState
    xor ecx, ecx
    lea eax, dword ptr ds:[edi+21h]
    .repeat
        movzx edx, byte ptr ds:[eax-1]
        movzx esi, byte ptr ds:[eax]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+1]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+2]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+4]
        mov dword ptr ss:[esp+ecx*4+18h], edx
        movzx edx, byte ptr ds:[eax+3]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+5]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+6]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+8]
        mov dword ptr ss:[esp+ecx*4+1Ch], edx
        movzx edx, byte ptr ds:[eax+7]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+9]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+0Ah]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+0Ch]
        mov dword ptr ss:[esp+ecx*4+20h], edx
        movzx edx, byte ptr ds:[eax+0Bh]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+0Dh]
        shl edx, 8
        or edx, esi
        movzx esi, byte ptr ds:[eax+0Eh]
        shl edx, 8
        or edx, esi
        mov dword ptr ss:[esp+ecx*4+24h], edx
        add ecx, 4
        add eax, 10h
    .until ecx >= 10h

    lea eax, dword ptr ss:[esp+38h]
    mov ebx, 10h
    .repeat
        mov ecx, dword ptr ds:[eax-20h]
        xor ecx, dword ptr ds:[eax+14h]
        mov ebp, dword ptr ds:[eax-1Ch]
        xor ebp, dword ptr ds:[eax+18h]
        xor ecx, dword ptr ds:[eax]
        xor ebp, dword ptr ds:[eax+4]
        mov edx, dword ptr ds:[eax-18h]
        mov esi, dword ptr ds:[eax-14h]
        xor ecx, edx
        xor ebp, esi
        rol ecx, 1
        rol ebp, 1
        mov dword ptr ds:[eax+24h], ebp
        mov ebp, dword ptr ds:[eax+1Ch]
        xor ebp, dword ptr ds:[eax-10h]
        mov dword ptr ds:[eax+20h], ecx
        xor ebp, dword ptr ds:[eax+8]
        add eax, 10h
        xor ebp, edx
        mov edx, dword ptr ds:[eax-1Ch]
        xor edx, dword ptr ds:[eax-4]
        rol ebp, 1
        xor edx, ecx
        xor edx, esi
        rol edx, 1
        sub ebx, 1
        mov dword ptr ds:[eax+18h], ebp
        mov dword ptr ds:[eax+1Ch], edx
    .until ebx == 00h

    mov esi, dword ptr ds:[edi+4]
    mov eax, dword ptr ds:[edi+8]
    mov ecx, dword ptr ds:[edi+0Ch]
    mov edx, dword ptr ds:[edi+10h]
    mov edi, dword ptr ds:[edi+14h]
    mov dword ptr ss:[esp+10h], edi
    mov dword ptr ss:[esp+14h], ebx
    .repeat
        mov ebx, eax
        mov edi, esi
        rol edi, 5
        mov ebp, ecx
        and ebp, eax
        not ebx
        and ebx, edx
        or ebx, ebp
        mov ebp, dword ptr ss:[esp+10h]
        add edi, ebx
        mov ebx, dword ptr ss:[esp+14h]
        add edi, dword ptr ss:[esp+ebx*4+18h]
        ror eax, 2
        lea edi, dword ptr ds:[edi+ebp+5A827998h]
        mov ebp, eax
        and ebp, esi
        mov dword ptr ss:[esp+10h], edx
        mov edx, esi
        not edx
        and edx, ecx
        or edx, ebp
        mov dword ptr ss:[esp+14h], edi
        rol edi, 5
        add edi, edx
        add edi, dword ptr ss:[esp+ebx*4+1Ch]
        mov edx, dword ptr ss:[esp+10h]
        ror esi, 2
        lea edx, dword ptr ds:[edi+edx+5A827998h]
        mov edi, eax
        mov dword ptr ss:[esp+10h], ecx
        mov ecx, dword ptr ss:[esp+14h]
        mov ebp, esi
        and ebp, ecx
        mov eax, ecx
        not eax
        and eax, edi
        or eax, ebp
        mov dword ptr ss:[esp+14h], edx
        rol edx, 5
        add edx, eax
        add edx, dword ptr ss:[esp+ebx*4+20h]
        mov eax, dword ptr ss:[esp+10h]
        lea eax, dword ptr ds:[edx+eax+5A827998h]
        mov edx, dword ptr ss:[esp+14h]
        mov ebp, esi
        ror ecx, 2
        mov dword ptr ss:[esp+14h], eax
        rol eax, 5
        mov dword ptr ss:[esp+10h], edi
        mov esi, edx
        not esi
        and esi, ebp
        mov edi, ecx
        and edi, edx
        or esi, edi
        add eax, esi
        add eax, dword ptr ss:[esp+ebx*4+24h]
        mov esi, dword ptr ss:[esp+10h]
        lea esi, dword ptr ds:[eax+esi+5A827998h]
        mov edi, ecx
        mov ecx, dword ptr ss:[esp+14h]
        ror edx, 2
        mov dword ptr ss:[esp+10h], ebp
        mov eax, ecx
        not eax
        mov dword ptr ss:[esp+14h], esi
        and eax, edi
        rol esi, 5
        mov ebp, edx
        and ebp, ecx
        or eax, ebp
        add esi, eax
        add esi, dword ptr ss:[esp+ebx*4+28h]
        mov eax, dword ptr ss:[esp+10h]
        add ebx, 5
        lea esi, dword ptr ds:[esi+eax+5A827998h]
        mov eax, dword ptr ss:[esp+14h]
        ror ecx, 2
        mov dword ptr ss:[esp+10h], edi
        mov dword ptr ss:[esp+14h], ebx
    .until ebx >= 14h

    mov ebx, 14h
    .repeat
        mov edi, esi
        rol edi, 5
        mov ebp, edx
        xor ebp, ecx
        xor ebp, eax
        add edi, ebp
        add edi, dword ptr ss:[esp+ebx*4+18h]
        mov ebp, dword ptr ss:[esp+10h]
        ror eax, 2
        lea edi, dword ptr ds:[edi+ebp+6ED9EBC1h]
        mov ebp, ecx
        xor ebp, eax
        xor ebp, esi
        mov dword ptr ss:[esp+14h], edi
        rol edi, 5
        add edi, ebp
        add edi, dword ptr ss:[esp+ebx*4+1Ch]
        ror esi, 2
        lea edx, dword ptr ds:[edi+edx+6ED9EBC1h]
        mov edi, eax
        xor eax, esi
        mov ebp, ecx
        mov ecx, dword ptr ss:[esp+14h]
        xor eax, ecx
        mov dword ptr ss:[esp+14h], edx
        rol edx, 5
        add edx, eax
        add edx, dword ptr ss:[esp+ebx*4+20h]
        ror ecx, 2
        lea eax, dword ptr ds:[edx+ebp+6ED9EBC1h]
        mov edx, dword ptr ss:[esp+14h]
        mov dword ptr ss:[esp+14h], eax
        rol eax, 5
        mov ebp, edi
        mov edi, esi
        xor esi, ecx
        xor esi, edx
        add eax, esi
        add eax, dword ptr ss:[esp+ebx*4+24h]
        ror edx, 2
        lea esi, dword ptr ds:[eax+ebp+6ED9EBC1h]
        mov eax, ecx
        mov ecx, dword ptr ss:[esp+14h]
        mov dword ptr ss:[esp+14h], esi
        mov ebp, eax
        rol esi, 5
        xor ebp, edx
        xor ebp, ecx
        add esi, ebp
        add esi, dword ptr ss:[esp+ebx*4+28h]
        add ebx, 5
        ror ecx, 2
        mov dword ptr ss:[esp+10h], eax
        mov eax, dword ptr ss:[esp+14h]
        lea esi, dword ptr ds:[esi+edi+6ED9EBC1h]
    .until ebx >= 28h

    mov dword ptr ss:[esp+14h], 28h
    .repeat
        mov edi, esi
        rol edi, 5
        add edi, dword ptr ss:[esp+10h]
        mov ebx, ecx
        or ebx, eax
        and ebx, edx
        mov ebp, ecx
        and ebp, eax
        or ebx, ebp
        mov ebp, dword ptr ss:[esp+14h]
        add ebx, dword ptr ss:[esp+ebp*4+18h]
        ror eax, 2
        mov dword ptr ss:[esp+10h], edx
        lea edi, dword ptr ds:[ebx+edi+8F1BBEDCh]
        mov edx, eax
        or edx, esi
        and edx, ecx
        mov ebp, edi
        rol edi, 5
        add edi, dword ptr ss:[esp+10h]
        mov ebx, eax
        and ebx, esi
        or edx, ebx
        mov ebx, dword ptr ss:[esp+14h]
        add edx, dword ptr ss:[esp+ebx*4+1Ch]
        ror esi, 2
        lea edx, dword ptr ds:[edx+edi+8F1BBEDCh]
        mov edi, esi
        mov dword ptr ss:[esp+10h], ecx
        mov ecx, ebp
        or edi, ecx
        and edi, eax
        mov ebp, esi
        and ebp, ecx
        or edi, ebp
        add edi, dword ptr ss:[esp+ebx*4+20h]
        mov dword ptr ss:[esp+14h], edx
        rol edx, 5
        add edx, dword ptr ss:[esp+10h]
        ror ecx, 2
        mov dword ptr ss:[esp+10h], eax
        mov eax, dword ptr ss:[esp+14h]
        lea edx, dword ptr ds:[edi+edx+8F1BBEDCh]
        mov edi, esi
        mov esi, ecx
        or esi, eax
        and esi, edi
        mov ebp, ecx
        and ebp, eax
        or esi, ebp
        add esi, dword ptr ss:[esp+ebx*4+24h]
        mov dword ptr ss:[esp+14h], edx
        rol edx, 5
        add edx, dword ptr ss:[esp+10h]
        ror eax, 2
        lea esi, dword ptr ds:[esi+edx+8F1BBEDCh]
        mov edx, eax
        mov ebp, ecx
        mov ecx, dword ptr ss:[esp+14h]
        or eax, ecx
        mov dword ptr ss:[esp+10h], edi
        and eax, ebp
        mov dword ptr ss:[esp+14h], esi
        mov edi, edx
        and edi, ecx
        rol esi, 5
        add esi, dword ptr ss:[esp+10h]
        or eax, edi
        add eax, dword ptr ss:[esp+ebx*4+28h]
        add ebx, 5
        lea esi, dword ptr ds:[eax+esi+8F1BBEDCh]
        mov eax, dword ptr ss:[esp+14h]
        ror ecx, 2
        mov dword ptr ss:[esp+10h], ebp
        mov dword ptr ss:[esp+14h], ebx
    .until ebx >= 3ch

    mov edi, 3ch
    mov dword ptr ss:[esp+14h], edi
    lea esp, dword ptr ss:[esp]
    .repeat
        mov ebp, edx
        xor ebp, ecx
        xor ebp, eax
        add ebp, dword ptr ss:[esp+edi*4+18h]
        mov edi, dword ptr ss:[esp+14h]
        mov ebx, esi
        rol ebx, 5
        add ebx, dword ptr ss:[esp+10h]
        ror eax, 2
        lea ebx, dword ptr ds:[ebx+ebp+0CA62F1D6h]
        mov dword ptr ss:[esp+10h], edx
        mov edx, ecx
        xor edx, eax
        xor edx, esi
        add edx, dword ptr ss:[esp+edi*4+1Ch]
        mov ebp, ebx
        rol ebx, 5
        add ebx, dword ptr ss:[esp+10h]
        ror esi, 2
        lea edx, dword ptr ds:[edx+ebx+0CA62F1D6h]
        mov ebx, eax
        xor eax, esi
        mov dword ptr ss:[esp+10h], ecx
        mov ecx, ebp
        xor eax, ecx
        add eax, dword ptr ss:[esp+edi*4+20h]
        mov ebp, edx
        rol edx, 5
        add edx, dword ptr ss:[esp+10h]
        ror ecx, 2
        lea eax, dword ptr ds:[eax+edx+0CA62F1D6h]
        mov edx, ebp
        mov dword ptr ss:[esp+10h], ebx
        mov ebx, esi
        xor esi, ecx
        xor esi, edx
        add esi, dword ptr ss:[esp+edi*4+24h]
        mov ebp, eax
        rol eax, 5
        add eax, dword ptr ss:[esp+10h]
        ror edx, 2
        lea esi, dword ptr ds:[esi+eax+0CA62F1D6h]
        mov eax, ecx
        mov ecx, ebp
        mov ebp, eax
        xor ebp, edx
        mov dword ptr ss:[esp+10h], ebx
        mov ebx, esi
        rol esi, 5
        add esi, dword ptr ss:[esp+10h]
        xor ebp, ecx
        add ebp, dword ptr ss:[esp+edi*4+28h]
        add edi, 5
        lea esi, dword ptr ds:[esi+ebp+0CA62F1D6h]
        ror ecx, 2
        mov ebp, eax
        mov dword ptr ss:[esp+10h], eax
        mov eax, ebx
        mov dword ptr ss:[esp+14h], edi
    .until edi >= 50h

    mov edi, dword ptr ss:[esp+160h]
    mov ebx, dword ptr ds:[edi+4]
    add ebx, esi
    mov esi, dword ptr ds:[edi+8]
    add esi, eax
    mov eax, dword ptr ds:[edi+0Ch]
    add eax, ecx
    and eax, 0FEFEFEFEh
    mov dword ptr ds:[edi+0Ch], eax
    mov eax, dword ptr ds:[edi+10h]
    add eax, edx
    and eax, 0FEFEFEFEh
    mov dword ptr ds:[edi+10h], eax
    mov eax, dword ptr ds:[edi+14h]
    and ebx, 0FEFEFEFEh
    and esi, 0FEFEFEFEh
    add eax, ebp
    and eax, 0FEFEFEFEh
    mov dword ptr ds:[edi+4], ebx
    mov dword ptr ds:[edi+8], esi
    mov dword ptr ds:[edi+14h], eax
    mov dword ptr ds:[edi+60h], 0
    pop edi
    pop esi
    pop ebp
    pop ebx
    add esp, 148h
    Ret
FL_HashRound endp

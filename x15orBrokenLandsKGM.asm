.code
Gen_x15orBrokenLandsKGM proc in_username: DWORD, out_serial: DWORD
    local NameLen: DWORD
    local DWORDForXTEA1: DWORD
    local DWORDForXTEA2: DWORD
    local NameCRC32: DWORD

    invoke RtlZeroMemory, offset x15BroLan_TStr, sizeof x15BroLan_TStr

    invoke MD5Init
    mov dword ptr ds:[eax], 42524F4Bh
    mov dword ptr ds:[eax+4], 454E2D4Ch
    mov dword ptr ds:[eax+8], 414E4453h
    mov dword ptr ds:[eax+0Ch], 48454845h
    invoke StrLen, in_username
    mov NameLen, eax
    invoke MD5Update, in_username, NameLen
    invoke MD5Final
    mov edx, eax
    mov eax, in_username
    movzx eax, byte ptr ds:[eax+2]
    lea ebx, dword ptr ds:[eax*8]
    sub ebx, eax
    lea eax, dword ptr ds:[ebx+ebx*2]
    xor eax, 4Ch
    shl eax, 8
    mov al, ah
    shl eax, 8
    mov al, ah
    shl eax, 8
    mov al, ah
    xor ecx, ecx
    .while ecx < MD5_DIGESTSIZE/4
        xor dword ptr ds:[edx+ecx*4], eax
        inc ecx
    .endw
    xor ecx, ecx
    mov esi, edx
    mov edi, offset x15BroLan_TStr
    .while ecx < MD5_DIGESTSIZE
        movzx eax, byte ptr ds:[esi+ecx]
        push ecx
        invoke wsprintf, edi, offset x15BroLan_02wsp, eax
        pop ecx
        add edi, 2
        inc ecx
    .endw
    xor ecx, ecx
    xor esi, esi
    xor edi, edi
    .while ecx < 8
        movzx eax, byte ptr ds:[x15BroLan_TStr+ecx]
        add eax, 26260h
        imul eax, NameLen
        lea eax, dword ptr ds:[eax+eax*4]
        lea esi, dword ptr ds:[esi+eax*8]
        movzx eax, byte ptr ds:[x15BroLan_TStr+8+ecx]
        add eax, 5050h
        imul eax, NameLen
        lea eax, dword ptr ds:[eax+eax*4]
        lea edi, dword ptr ds:[edi+eax]
        lea edi, dword ptr ds:[edi+eax*4]
        inc ecx
    .endw
    mov DWORDForXTEA1, esi
    mov DWORDForXTEA2, edi
    mov eax, DWORDForXTEA1
    mov dword ptr ds:[x15BroLan_TStr], eax
    mov eax, DWORDForXTEA2
    mov dword ptr ds:[x15BroLan_TStr+4], eax
    invoke XTEAEncypher, 1Eh, offset x15BroLan_TStr, offset x15BroLan_Key
    mov eax, dword ptr ds:[x15BroLan_TStr]
    mov ebx, dword ptr ds:[x15BroLan_TStr+4]
    invoke wsprintf, out_serial, offset x15BroLan_08wsp, eax, ebx
    mov eax, out_serial
    add eax, 10h
    mov byte ptr ds:[eax], 2Dh
    invoke CRC32, in_username, NameLen, 00h
    mov NameCRC32, eax
    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset x15BroLan_D, BigD
    invoke bnFromHex, offset x15BroLan_N, BigN
    invoke bnFromBytes, addr NameCRC32, 4, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    mov eax, out_serial
    add eax, 11h
    invoke bnToHex, BigM, eax
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    invoke StrLen, out_serial
    add eax, out_serial
    mov byte ptr ds:[eax], 2Dh
    invoke szCatStr, out_serial, offset x15BroLan_Key
    Ret
Gen_x15orBrokenLandsKGM endp

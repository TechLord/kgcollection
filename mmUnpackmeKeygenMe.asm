.code
Gen_mmUnpackmeKeygenMe proc in_username: DWORD, out_serial: DWORD
    invoke StrLen, in_username
    mov esi, eax
    mov edi, in_username
    xor edx, edx

    .while edx < esi
        mov al, byte ptr ds:[edi+edx]
        mov cl, al
        add cl, 59h
        xor cl, al
        xor cl, 78h
        mov al, cl
        xor al, 45h
        add al, cl
        add al, 67h
        sar al, 03h
        mov cl, al
        add cl, 59h
        xor cl, al
        xor cl, 78h
        mov al, cl
        xor al, 0C5h
        add al, cl
        sub al, 19h
        and al, 78h
        mov byte ptr ds:[mmKM_TempString+edx], al
        inc edx
    .endw

    invoke HexEncode, offset mmKM_TempString, esi, out_serial
    Ret
Gen_mmUnpackmeKeygenMe endp
